%define _bindir /bin
Name:            setserial
Version:         2.17
Release:         53
Summary:         A utility for configuring serial ports
License:         GPL+
URL:             http://setserial.sourceforge.net
Source0:         https://sourceforge.net/projects/setserial/files/setserial/%{version}/%{name}-%{version}.tar.gz

Patch0:          setserial-2.17-fhs.patch
Patch1:          setserial-2.17-rc.patch
Patch2:          setserial-2.17-spelling.patch
Patch3:          setserial-hayesesp.patch
Patch4:          setserial-aarch64.patch

BuildRequires:   gcc groff 

%description
setserial is a program designed to set and/or report the
configuration information associated with a serial port.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%set_build_flags
CFLAGS="$CFLAGS $LDFLAGS"
%configure
%make_build

%install
install -d %{buildroot}/%{_bindir}
install -d %{buildroot}/%{_mandir}/man8

%make_install

%files
%defattr(-,root,root)
%doc  rc.serial
%{_bindir}/setserial

%files help
%defattr(-,root,root)
%doc README
%{_mandir}/man*/*.gz

%changelog
* Fri Oct 21 2022 zhouyihang <zhouyihang3@h-partners.com> - 2.17-53
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix wrong date in changelog

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 2.17-52
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Tue Dec 15 2020 xihaochen <xihaochen@huawei.com> - 2.17-51
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update url

* Fri Jan 3 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.17-50
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:optimization the spec

* Mon Oct 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.17-49
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:remove comments in spec

* Fri Sep 06 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.17-48
- Package init

